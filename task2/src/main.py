from fastapi import FastAPI
import json

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/smartphones/")
def get_smartphones(price: str):
    file = open('smartphones.json')
    my_dict = json.load(file)
    req_dict = []
    for i in my_dict:
        if i['price'] == price:
            req_dict.append(i)
    if len(req_dict) == 0:
        return {"We could not find any smartphone for ": price}
    return req_dict
