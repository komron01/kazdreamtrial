import requests
from bs4 import BeautifulSoup
import json

link = 'https://shop.kz/smartfony/filter/almaty-is-v_nalichii-or-ojidaem-or-dostavim/apply'

headers = {'Accept-Language': 'en-US,en;q=0.9,ru;q=0.8',
           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                         'Chrome/109.0.0.0 Safari/537.36 Edg/109.0.1518.55'}

response = requests.get(link, headers=headers)
soup = BeautifulSoup(response.content, 'html.parser')
catalog_items = soup.find_all('div', class_='bx_catalog_item_container gtm-impression-product')


js_list = []

for item in catalog_items:
    dicto = item.get('data-product')
    data = json.loads(dicto)
    name = data['item_name']
    articul = data['item_id']
    price = data['price']
    # С памятью смартфона немного замудрил мне кажется, но максимально близкое решение такое
    memory_div = item.findChildren('div', class_='bx_catalog_item_articul')[0]
    memory = memory_div.find_all('span', attrs={"data-prop-title": '386'})  # memory index
    memory = memory[0].find_next_sibling('span').getText()  # memory value

    temp_dict = {'name': name,
                 'articul': str(articul),
                 'price': str(price),
                 'memory-size': memory}

    js_list.append(temp_dict)


json_object = json.dumps(js_list, indent=0)
with open("smartphones.json", "w") as outfile:
    outfile.write(json_object)
